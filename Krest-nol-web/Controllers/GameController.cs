﻿using System;
using System.Web;
using System.Web.Mvc;
using Krest_nol_core;
using Krest_nol_web.Models.Game;
using Krest_nol_web.Models.Game.Load;

namespace Krest_nol_web.Controllers
{
    public class GameController : Controller
    {
        public ActionResult Starter()
        {
            return View();
        }


        [HttpGet]
        public ActionResult LoadBdGame()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadBdGame(LoadBd data)
        {
            Game game = new Game();
                      
            if (data.Input == null)
            {
                ModelState.AddModelError("myError", "Не введён ID");
                return View();
            }
            int id;
            Int32.TryParse(data.Input, out id);
            WorkingBd.Load(ref game, id);
            GameM gameModel = new GameM { Game = game };
            Session.Set(new SessionObject {GameModel = gameModel, IsGameStart = true});
            return RedirectToAction("Game");
        }


        [HttpGet]
        public ActionResult Game()
        {
            return View(Session.Get() == null ? null : Session.Get().GameModel);
        }

        [HttpPost]
        public ActionResult Game(string btn, string save)
        {
            GameM gameModel = Session.Get().GameModel;
            if (gameModel == null)
                return View(Session.Get().GameModel);
            Game game = gameModel.Game;
            if (game == null) 
                return View(Session.Get().GameModel);
            if (game.Victory == null)
                game.Victory = new Victory(game);
            if (game.Victory.IsEndOfGame)
                return View("Victory");
            if (save != null)
                switch (save)
                {
                    case "File":
                        return View(gameModel);
                    case "BD":
                        WorkingBd.Save(game);
                        return View(gameModel);
                    default:
                        break;
                }
            string pole = game.Pole;
            if (!game.CurrentPlayers[game.CurrentPlayer].MakeAMove(Cell.Parse(btn), ref pole, game.SizePole))
                return View(Session.Get().GameModel);
            game.Pole = pole;
            game.Victory.CalculateVictory(Cell.Parse(btn));
            if (game.Victory.IsEndOfGame)
                return View("Victory");
            game.CurrentPlayer++;
            return View(Session.Get().GameModel);
        }


        [HttpGet]
        public ActionResult NewGame()
        {
            return View();
        }

        [HttpPost]
        public ActionResult NewGame(NewGame newGame)
        {
            Game game = new Game
            {
                SizePole = newGame.Size,
                WinSequenceLength = newGame.WinSequenceLength,
                PlayerCount = newGame.PlayerCount,
                CurrentPlayers = new Player[newGame.PlayerCount],
                CurrentPlayer = 0,
                Pole = new string(Krest_nol_core.Game.DefaultCellsPole, newGame.Size * newGame.Size)
            };

            GameM gameModel = new GameM {Game = game};

            SessionObject sessionObject = new SessionObject
            {
                GameModel = gameModel,
                NewGame = newGame
            };

            Session.Set(sessionObject);

            return RedirectToAction("Players");
        }

        [HttpGet]
        public ActionResult Players()
        {
            return View(Session.Get().NewGame);
        }

        [HttpPost]
        public ActionResult Players(NewGame nGame)
        {
            Game game = Session.Get().GameModel.Game;
            NewGame newGame = Session.Get().NewGame;
            if (newGame.PlayerCount <= 0)
            {
                WorkingBd.Save(game);
                return RedirectToAction("Game");
            }
            Player player;
            WorkingBd.LoadPlayerByName(out player, nGame.PlayerName);
            if (player == null)
            {
                player = new Player
                {
                    Name = nGame.PlayerName,
                    Symbol = nGame.PlayerSymbol
                };
                WorkingBd.InsertPlayer(player);
            }
            game.CurrentPlayers[game.PlayerCount-newGame.PlayerCount] = player;
            newGame.PlayerCount--;

            SessionObject so = Session.Get();
            so.NewGame = newGame;
            so.GameModel.Game = game;


            if (newGame.PlayerCount > 0)
            {
                Session.Set(so);
                return View(newGame);
            }
            WorkingBd.Save(game);
            so.IsGameStart = true;
            Session.Set(so);
            return RedirectToAction("Game");

        }

        public ActionResult LoadGame()
        {
            //Session.Get().Set();

            return View();
        }

        public ActionResult LoadFileGame()
        {
            return View();
        }

        public ActionResult Victory()
        {
            return View();
        }
	}
}

public class SessionObject
{
    internal const string ObjectName = "kjbkjfgbh";
    public GameM GameModel { get; set; }

    public NewGame NewGame { get; set; }

    public bool IsGameStart { get; set; }
}

public static class SessionExt
{
    public static SessionObject Get(this HttpSessionStateBase session)
    {
        return (SessionObject)session[SessionObject.ObjectName];
    }

    public static void Set(this HttpSessionStateBase session, SessionObject objects)
    {
        session[SessionObject.ObjectName] = objects;
    }
}