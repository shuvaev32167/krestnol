﻿using System.Web.Mvc;
using Krest_nol_core;
using Krest_nol_web.Models;

namespace Krest_nol_web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(string id)
        {

            if (id != null) Session["ID"] = id;
            return View(Session["ID"]);
        }

        [HttpGet]
        public ActionResult InputResult()
        {
            return View();
        }

        [HttpPost]
        public ActionResult InputResult(InputData data)
        {
            if (ModelState.IsValid)
            {
                if (data.IsValid())
                {
                    return View("SuccessResult");
                }
                ModelState.AddModelError("myError", "Error text");
            }
            return View(data);
        }

        [HttpGet]
        public ActionResult Game()
        {
            GameModel date = new GameModel();
            Game game = new Game();
            Session["GameModel"] = date;
            Session["game"] = game;
            date.Game = game;
            return View();
        }

        [HttpPost]
        public ActionResult Game(GameModel data)
        {
            var game = Session["GameModel"] as GameModel;
            if (game == null) return View();

            if (!game.IsCorrectInputStage(data.InputData))
                ModelState.AddModelError("myError", game.ErrorText);

            return View(game);
        }

        public JsonResult GetJson()
        {
            return Json(Session["GameModel"], JsonRequestBehavior.AllowGet);
        }
    }
}