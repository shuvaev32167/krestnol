﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Krest_nol_web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Game", action = "Starter", id = UrlParameter.Optional }
            );
        }
    }
}
