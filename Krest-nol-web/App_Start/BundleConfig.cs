﻿using System.Web;
using System.Web.Optimization;

namespace Krest_nol_web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/bundles/Bootstrap.css")
                .Include("~/Content/bootstrap/css/bootstrap.css")
                .Include("~/Content/bootstrap/css/bootstrap-theme.css"));

            bundles.Add(new ScriptBundle("~/bundles/Bootstrap.js").Include(
                        "~/Content/bootstrap/js/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));
        }
    }
}
