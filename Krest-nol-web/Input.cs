﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Krest_nol_core;
using PublicLibrary;

namespace Krest_nol_web
{
    public class Input
    {
        private const char SeparatorParts = ' ';
        public enum Answer
        {
            Ok = 0,
            Error = 1,
            ActionExternalFile = 2,
            Save = 3,
            Load = 4,
            Exit = 5,
            SaveBd = 6,
            LoadBd = 7,
            Stat = 8
        }

        private static bool ActionExternalFile(string command, string path, out Answer answer, ref Game game)
        {
            answer = Answer.Error;
            if (command.Length < 4) return false;
            
            Answer result;
            if (!Enum.TryParse(command.ToLower(), true, out result)) return false;
            switch (result)
            {
                case Answer.Save:
                    ExternalFile.Save(path, game);
                    answer = Answer.Save;
                    return true;
                case Answer.Load:
                    ExternalFile.Load(path, ref game);
                    answer = Answer.Load;
                    game.LoadGame();
                    return true;
                case Answer.Exit:
                    Environment.Exit(0);
                    // ReSharper disable once HeuristicUnreachableCode
                    return true;
                case Answer.SaveBd:
                    answer = Answer.Save;
                    WorkingBd.Save(game);
                    return true;
                case Answer.LoadBd:
                    answer = Answer.Load;
                    int id;
                    Int32.TryParse(path, out id);
                    WorkingBd.Load(ref game, id);
                    game.LoadGame();
                    return true;
                case Answer.Stat:
                    answer = Answer.Stat;
                    Console.WriteLine("Число игроков = {0}\nЛучший игрок:\n{1}\nСумма побед = {2}", Stat.CountPlayer(game)
                        , Stat.BestSeries(game), Stat.SumVictory(game));
                    return true;
                default:
                    try
                    {
                        Assembly ass = Assembly.Load("Addon");
                        Type[] types = ass.GetTypes();

                        IEnumerable<Type> istats = from type in types
                            let inter = type.GetInterfaces()
                            from info in inter
                            where info.FullName == typeof (IStat).FullName
                            select type;
                        foreach (Type istat in istats)
                        {
                            var addon = Activator.CreateInstance(istat) as IStat;
                            if (addon != null) addon.StatSave(game, path);

                            answer = Answer.Stat;
                            return true;
                        }
                        return false;
                    }
                    catch (Exception)
                    {
                        return false;
                    }
            }
        }

        public static Answer ParseInput(string input, out int value, ref Game game)
        {
            string[] bufStrings = input.Split(SeparatorParts);
            Answer answer;
            if (ActionExternalFile(bufStrings.First(), bufStrings.Last(), out answer, ref game))
            {
                value = 0;
                return answer;
            }
            if (bufStrings.Count() == 1)
            {
                return Int32.TryParse(input, out value) ? Answer.Ok : Answer.Error;
            }
            value = 0;
            return Answer.Error;
        }

        public static Answer ParseInput(string input, ref Point value, ref Game game)
        {
            string[] bufStrings = input.Split(SeparatorParts);
            Answer answer;
            if (ActionExternalFile(bufStrings.First(), bufStrings.Last(), out answer, ref game))
            {
                value = null;
                return answer;
            }
            if (bufStrings.Count() != 2)
            {
                value = null;
                return Answer.ActionExternalFile;
            }
            int[] bufInts = new int[2];
            if (Int32.TryParse(bufStrings.First(), out bufInts[0]))
            {
                Int32.TryParse(bufStrings.Last(), out bufInts[1]);
                value = new Point(bufInts);
                return Answer.Ok;
            }
            return Answer.Error;
        }

        public static Answer ParseInput(string input, out string value, ref Game game)
        {
            string[] bufStrings = input.Split(SeparatorParts);
            Answer answer;
            if (ActionExternalFile(bufStrings.First(), bufStrings.Last(), out answer, ref game))
            {
                value = "";
                return answer;
            }
            value = input;
            return Answer.Ok;
        }
    }
}
