﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using Krest_nol_core;
using PublicLibrary;

namespace Krest_nol_web.Models
{
    public class GameModel
    {
        private const string SizePole = "Введите размер поля: ",
            ChislPovtor = "Введите число повторений в ряду:",
            SizePlayer = "Введите число игроков:";

        private const string HorizontalBorder = " -";
        private const string VerticalBorder = "|";
        private const int MaxPole = 10, MinPole = 2, MinPlayer = 1;
        private const char DefaultCellsPole = ' ';

        public Krest_nol_core.Game Game { get; set; }

        [Required(ErrorMessage = "Не заполнено")]
        // [LoginExistsAttribute(ErrorMessage = "Login exists")]
        public string Output
        {
            get
            {
                return _output;
            }
            set
            {
                _output = value;
            }
        }

        private string _output;

        public string InputData { get; set; }

        public int Stage { get; set; }

        public string ErrorText { get; set; }

        public GameModel()
        {
            Stage = 0;
            _output = SizePole;
        }

        public GameModel(string inputData)
        {
            InputData = inputData;
        }

        public bool IsCorrectInputStage(string input)
        {
            Krest_nol_core.Game game;
  
            switch (Stage)
            {
                case 0:
                    int sizePole;
                    game = Game;
                    /*Input.*/Answer parceAnsverResult = /*Input.*/ParseInput(input, out sizePole, ref game);
                    Game = game;
                    while (sizePole < MinPole || sizePole > MaxPole)
                    {
                        if (parceAnsverResult == /*Input.*/Answer.Load)
                        {
                            StartGame();
                            return true;
                        }
                        if (parceAnsverResult == /*Input.*/Answer.Ok || parceAnsverResult == /*Input.*/Answer.Error)
                        {
                            ErrorText = "Не верные размеры поля";
                            return false;
                        }
                        _output = "Введите размер поля: ";
                        ErrorText = null;
                        return false;
                    }
                    Game.SizePole = sizePole;
                    _output = ChislPovtor;
                    Stage ++;
                    return true;
                case 1:
                    int winSequenceLength;
                    game = Game;
                    parceAnsverResult = /*Input.*/ParseInput(input, out winSequenceLength, ref game);
                    Game = game;
                    if (winSequenceLength < MinPole || winSequenceLength > Game.SizePole)
                    {
                        if (parceAnsverResult == /*Input.*/Answer.Load)
                        {
                            StartGame();
                            return true;
                        }
                        if (parceAnsverResult == /*Input.*/Answer.Ok || parceAnsverResult == /*Input.*/Answer.Error)
                        {
                            ErrorText = "Не верное число повторений";
                            return false;
                        }
                        ErrorText = null;
                        _output = "Введите число повторений в ряду: ";
                        return false;
                    }
                    Game.WinSequenceLength = winSequenceLength;
                    _output = SizePlayer;
                    Stage++;
                    return true;
                case 2:
                    int playerCount;
                    game = Game;
                    parceAnsverResult = /*Input.*/ParseInput(input, out playerCount, ref game);
                    Game = game;
                    if (playerCount < MinPlayer || playerCount > (Game.SizePole * Game.SizePole) / Game.WinSequenceLength)
                    {
                        if (parceAnsverResult == /*Input.*/Answer.Load)
                        {
                            StartGame();
                            return true;
                        }
                        if (parceAnsverResult == /*Input.*/Answer.Ok || parceAnsverResult == /*Input.*/Answer.Error)
                        {
                            ErrorText = "Не верное число игроков";
                            return false;
                        }
                        _output = "Введите число игроков: ";
                        ErrorText = null;
                        return false;
                    }
                    Game.PlayerCount = playerCount;
                    StartGame();
                    return true;
                case 10:
                    Point pos = null;
                    game = Game;
                    /*Input.*/Answer parceAnswerResult = /*Input.*/ParseInput(input, ref pos, ref game);
                    Game = game;
                    bool isCorrectCoordinatsY = pos != null && (pos.Y >= 0 && pos.Y < Game.SizePole);
                    bool isCorrectCoordinatsX = pos != null && (pos.X >= 0 && pos.X < Game.SizePole);
                    bool isCorrectCoordinats = isCorrectCoordinatsX && isCorrectCoordinatsY;
                    bool isCorrectPos = true;
                    if (isCorrectCoordinats)
                        isCorrectPos = (Game.Pole[pos.Y * Game.SizePole + pos.X] == DefaultCellsPole);
                    if (!isCorrectCoordinats || !isCorrectPos)
                    {
                        bool isAnswerGood = (parceAnswerResult == /*Input.*/Answer.Ok ||
                                         parceAnswerResult == /*Input.*/Answer.Error);
                        if (isAnswerGood && !isCorrectCoordinats)
                        {
                            ErrorText = "Не верные координаты";
                            return false;
                        }
                        if (parceAnswerResult != /*Input.*/Answer.Stat)
                        {
                            if (!isCorrectPos)
                            {
                                ErrorText = "Ячейка занята";
                                return false;
                            }
                            ErrorText = null;
                            _output = DisplayPole() + "<br/>" +
                                      String.Format("Ходит {0}\r\n", Game.CurrentPlayers[Game.CurrentPlayer].Name);
                            return false;
                        }
                    }

                    string pole = Game.Pole;
                    Game.CurrentPlayers[Game.CurrentPlayer].MakeAMove(pos, ref pole, Game.SizePole);
                    Game.Pole = pole;
                    Game.Victory.CalculateVictory(pos);
                    Game.CurrentPlayer++;

                    if (Game.Victory.IsEndOfGame)
                    {
                        Stage=20;
                        return true;
                    }
                    ErrorText = null;
                    return false;
                default:
                    return false;
            }
        }

        public void StartGame()
        {
            Stage = 9;
            ErrorText = null;
            _output = DisplayPole();
            _output += "\n" + String.Format("Ходит {0}", Game.CurrentPlayers[Game.CurrentPlayer].Name);
            if (Game.Victory == null)
                Game.Victory = new Victory(Game);
        }

        private string DisplayPole()
        {
            var buf = new StringBuilder();
            for (int j = 0; j < Game.SizePole; j++)
                buf.Append(HorizontalBorder);
            buf.Append("<br/>");
            for (int i = 0; i < Game.SizePole; i++)
            {
                buf.Append(VerticalBorder);
                for (int j = 0; j < Game.SizePole; j++)
                    buf.Append(Game.Pole[i * Game.SizePole + j] + VerticalBorder);
                buf.Append("<br/>");
                for (int j = 0; j < Game.SizePole; j++)
                    buf.Append(HorizontalBorder);
                buf.Append("<br/>");
            }
            return buf.ToString();
        }

        private const char SeparatorParts = ' ';
        public enum Answer
        {
            Ok = 0,
            Error = 1,
            ActionExternalFile = 2,
            Save = 3,
            Load = 4,
            Exit = 5,
            SaveBd = 6,
            LoadBd = 7,
            Stat = 8
        }


        private bool ActionExternalFile(string command, string path, out Answer answer, ref Krest_nol_core.Game game)
        {
            answer = Answer.Error;
            if (command.Length < 4) return false;

            Answer result;
            if (!Enum.TryParse(command.ToLower(), true, out result)) return false;
            switch (result)
            {
                case Answer.Save:
                    ExternalFile.Save(path, game);
                    answer = Answer.Save;
                    return true;
                case Answer.Load:
                    ExternalFile.Load(path, ref game);
                    answer = Answer.Load;
                    game.LoadGame();
                    return true;
                case Answer.Exit:
                    Environment.Exit(0);
                    // ReSharper disable once HeuristicUnreachableCode
                    return true;
                case Answer.SaveBd:
                    answer = Answer.Save;
                    WorkingBd.Save(game);
                    return true;
                case Answer.LoadBd:
                    answer = Answer.Load;
                    int id;
                    Int32.TryParse(path, out id);
                    WorkingBd.Load(ref game, id);
                    game.LoadGame();
                    return true;
                case Answer.Stat:
                    answer = Answer.Stat;
                    Console.WriteLine("Число игроков = {0}\nЛучший игрок:\n{1}\nСумма побед = {2}", Stat.CountPlayer(game)
                        , Stat.BestSeries(game), Stat.SumVictory(game));
                    return true;
                default:
                    try
                    {
                        Assembly ass = Assembly.Load("Addon");
                        Type[] types = ass.GetTypes();

                        IEnumerable<Type> istats = from type in types
                                                   let inter = type.GetInterfaces()
                                                   from info in inter
                                                   where info.FullName == typeof(IStat).FullName
                                                   select type;
                        foreach (Type istat in istats)
                        {
                            var addon = Activator.CreateInstance(istat) as IStat;
                            if (addon != null) addon.StatSave(game, path);

                            answer = Answer.Stat;
                            return true;
                        }
                        return false;
                    }
                    catch (Exception)
                    {
                        return false;
                    }
            }
        }

        public Answer ParseInput(string input, out int value, ref Krest_nol_core.Game game)
        {
            string[] bufStrings = input.Split(SeparatorParts);
            Answer answer;
            if (ActionExternalFile(bufStrings.First(), bufStrings.Last(), out answer, ref game))
            {
                value = 0;
                return answer;
            }
            if (bufStrings.Count() == 1)
            {
                return Int32.TryParse(input, out value) ? Answer.Ok : Answer.Error;
            }
            value = 0;
            return Answer.Error;
        }

        public Answer ParseInput(string input, ref Point value, ref Krest_nol_core.Game game)
        {
            string[] bufStrings = input.Split(SeparatorParts);
            Answer answer;
            if (ActionExternalFile(bufStrings.First(), bufStrings.Last(), out answer, ref game))
            {
                value = null;
                return answer;
            }
            if (bufStrings.Count() != 2)
            {
                value = null;
                return Answer.ActionExternalFile;
            }
            int[] bufInts = new int[2];
            if (Int32.TryParse(bufStrings.First(), out bufInts[0]))
            {
                Int32.TryParse(bufStrings.Last(), out bufInts[1]);
                value = new Point(bufInts);
                return Answer.Ok;
            }
            return Answer.Error;
        }

        public Answer ParseInput(string input, out string value, ref Krest_nol_core.Game game)
        {
            string[] bufStrings = input.Split(SeparatorParts);
            Answer answer;
            if (ActionExternalFile(bufStrings.First(), bufStrings.Last(), out answer, ref game))
            {
                value = "";
                return answer;
            }
            value = input;
            return Answer.Ok;
        }


    }
}