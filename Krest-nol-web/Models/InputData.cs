﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Krest_nol_web.Models
{
    public class InputData
    {
        [Required(ErrorMessage = "Insert name")]
        [StringLength(50, MinimumLength = 4, ErrorMessage = "Name length is out of range")]
        [Display(Name = "Your name:")]
        [LoginExistsAttribute(ErrorMessage = "Login exists")]
        public string Name { get; set; }
        [Display(Name = "Your common browser:")]
        public string Browser { get; set; }
        [Display(Name = "Comment:")]
        public string Comment { get; set; }

        public InputData()
        {
        }

        public InputData(string name, string browser, string comment)
        {
            Name = name;
            Browser = browser;
            Comment = comment;
        }

        public override string ToString()
        {
            return String.Format("Name: {0}; Browser: {1}; Comment: {2}", Name, Browser, Comment);
        }

        public bool IsValid()
        {
            if (Name != null &&
                Browser != null &&
                Comment != null)
                return true;
            return false;
        }
    }

    public class LoginExistsAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            return value as string == "admin" ? new ValidationResult(ErrorMessage) : null;
        }
    }
}