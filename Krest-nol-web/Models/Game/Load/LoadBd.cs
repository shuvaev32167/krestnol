﻿using System.ComponentModel.DataAnnotations;

namespace Krest_nol_web.Models.Game.Load
{
    public class LoadBd
    {
        [Required(ErrorMessage = "Введите ID-шник")]
        [Display(Name = "Введите ID:")]
        public string Input { get; set; }
    }
}