﻿using System.ComponentModel.DataAnnotations;
using Krest_nol_core;

namespace Krest_nol_web.Models.Game
{
    public class NewGame
    {
        private const int MaxPole = 10, MinPole = 2, MinPlayer = 1;

        [Required(ErrorMessage = "Введите размер поля")]
        [Display(Name = "Введите размер поля:")]
        public int Size { get; set; }

        [Required(ErrorMessage = "Введите число повторений в ряд")]
        [Display(Name = "Введите число повторений в ряд:")]
        public int WinSequenceLength { get; set; }

        [Required(ErrorMessage = "Введите число игроков")]
        [Display(Name = "Введите число игроков:")]
        public int PlayerCount { get; set; }

        [Required(ErrorMessage = "Введите имя игрока")]
        [Display(Name = "Введите имя игрока:")]
        public string PlayerName { get; set; }

        [Display(Name = "Введите символ игрока (для новых игроков):")]
        public char PlayerSymbol { get; set; }
    }
}