﻿using System;

namespace Krest_nol_core
{
    public partial class Player
    {
        //public string NamePlayer { get; set; }
        //public char SymbolPlayer { get; set; }
        public void MakeAMove(Cell point, ref string pole, int sizePole)
        {
            if (String.IsNullOrEmpty(pole))
                throw new ArgumentNullException();
            if (point.Col > sizePole || point.Col < 0)
                throw new FormatException();
            if (point.Row > sizePole || point.Row < 0)
                throw new FormatException();
            char[] ppole = pole.ToCharArray();
            ppole[point.Row * sizePole + point.Col] = Symbol;
            pole = "";
            for (int i = 0, size = ppole.Length; i < size; i++)
                pole += ppole[i];
            //pole = Convert.ToString(ppole);
        }

        /*public uint CountGame { get; set; }
        public uint CountVictory { get; set; }
        public uint CurrentSeries { get; set; }
        public uint BestSeries { get; set; }*/
    }
}
