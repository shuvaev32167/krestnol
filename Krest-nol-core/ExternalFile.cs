﻿using System.IO;
using Newtonsoft.Json;

namespace Krest_nol_core
{
    public static class ExternalFile
    {
        public static void Save(string put, Game data)
        {
            FileStream fs = File.Create(put);
// ReSharper disable once InconsistentNaming
            SaveBuf Data = new SaveBuf(data);
            string s = JsonConvert.SerializeObject(Data);
            StreamWriter stream = new StreamWriter(fs);
            stream.Write(s);
            stream.Close();
            fs.Close();
        }

        public static void Load(string put, ref Game data)
        {
            if (data == null) 
                data = new Game();
            using (FileStream fs = File.OpenRead(put))
            {
                StreamReader streamReader = new StreamReader(fs);
                string s = streamReader.ReadToEnd();
                SaveBuf buf = JsonConvert.DeserializeObject<SaveBuf>(s);
                data = buf;
            }

            WorkingBd.Save(data);
        }
    }
}
