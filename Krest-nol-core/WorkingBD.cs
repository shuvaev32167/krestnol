﻿using System;
using System.Data.Linq;
using System.Linq;

namespace Krest_nol_core
{
    public static class WorkingBd
    {
        public static void Save(Game game)
        {
            if (game == null) throw new ArgumentNullException("game");
            game.CurrentPlayerID = game.CurrentPlayers[game.CurrentPlayer].ID;

            DataBase dataBase = DataBase.DB;
            KrestNolDataContext krestNolData = dataBase.KrestNol;
            //TODO
            bool gameNotExists = krestNolData.Games.All(g => g.ID != game.ID);
            if (gameNotExists)
            {
                krestNolData.Games.InsertOnSubmit(game);
            }
            krestNolData.SubmitChanges();
            foreach (Player player in game.CurrentPlayers)
            {
                //TODO
                bool playerNotExists = !krestNolData.Players.Any(p => p.ID == player.ID);
                if (playerNotExists)
                {
                    krestNolData.Players.InsertOnSubmit(player);
                }
                krestNolData.SubmitChanges();
                //TODO
                PlayerGame playerGame = krestNolData.PlayerGames.SingleOrDefault(pg => pg.IDPlayer == player.ID &&
                                                                              pg.IDGame == game.ID);
                if (playerGame == null)
                {
                    playerGame = new PlayerGame
                    {
                        IDGame = game.ID,
                        IDPlayer = player.ID
                    };
                    krestNolData.PlayerGames.InsertOnSubmit(playerGame);
                    krestNolData.SubmitChanges();
                }
                Console.WriteLine(game.ID);
            }
            krestNolData.SubmitChanges();
        }

        public static void Load(ref Game game, int id)
        {
            DataBase dataBase = DataBase.DB;
            dataBase.RefreshDataContext();
            KrestNolDataContext krestNolData = dataBase.KrestNol;
            //TODO
            game = krestNolData.Games.SingleOrDefault(g => g.ID == id);
            krestNolData.Refresh(RefreshMode.OverwriteCurrentValues, krestNolData.Games);
            if (game != null)
            {
                int size = game.PlayerGames.Count;
                PlayerGame[] playerGames = game.PlayerGames.ToArray();
                game.CurrentPlayers = new Player[size];
                for (int i = 0; i < size; i++)
                {
                    //TODO
                    game.CurrentPlayers[i] = krestNolData.Players.Single(p => p.ID == playerGames[i].IDPlayer);
                    if (game.CurrentPlayerID == game.CurrentPlayers[i].ID)
                        game.CurrentPlayer = i;
                }
            }
        }

        public static void LoadPlayerByName(out Player player, string name)
        {
            DataBase dataBase = DataBase.DB;
            dataBase.RefreshDataContext();
            KrestNolDataContext krestNolData = dataBase.KrestNol;
            player = krestNolData.Players.FirstOrDefault(x => x.Name == name);
        }

        public static void InsertPlayer(Player player)
        {
            DataBase dataBase = DataBase.DB;
            dataBase.RefreshDataContext();
            KrestNolDataContext krestNolData = dataBase.KrestNol;
            krestNolData.Players.InsertOnSubmit(player);
            krestNolData.SubmitChanges();
        }
    }
}