﻿using System;
using System.Linq;

namespace Krest_nol_core
{
    public class Cell
    {
        public const char Separator = ' ';

        public int Col { get; private set; }
        public int Row { get; private set; }
        public Cell(int x, int row)
        {
            Col = x;
            Row = row;
        }

        public Cell(int[] value)
        {
            if (value.Count() == 2)
            {
                Col = value.Last();
                Row = value.First();
            }
        }

        private Cell()
        {
            //throw new NotImplementedException();
        }

        public override string ToString()
        {
            return String.Format("{0}{2}{1}",Row,Col,Separator);
        }

        public static Cell Parse(string source)
        {
            if (source == null)
                throw new ArgumentNullException();
            string[] parameters = source.Split(Separator);
            if (parameters.Count() != 2)
                throw new FormatException();
            Cell cell = new Cell
            {
                Col = Int32.Parse(parameters[1]), 
                Row = Int32.Parse(parameters[0])
            };
            return cell;
        }

        public static bool TryParse(string source, out Cell result)
        {
            if (source == null)
            {
                result = null;
                return false;
            }
            string[] parameters = source.Split(Separator);
            if (parameters.Count() != 2)
            {
                result = null;
                return false;
            }
            int row, col;
            if (!(Int32.TryParse(parameters[0], out row) && Int32.TryParse(parameters[1], out col)))
            {
                result = null;
                return false;
            }

            result = new Cell
            {
                Col = col,
                Row = row
            };

            return true;
        }
    }
}
