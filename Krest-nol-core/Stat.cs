﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Krest_nol_core
{
    public static class Stat
    {
        public static string CountPlayer(Game game)
        {
            return game.PlayerGames.Count.ToString(CultureInfo.InvariantCulture);
        }

        public static string BestSeries(Game game)
        {
            IEnumerable<Player> players = from pg in game.PlayerGames
                orderby pg.Player.BestSeries descending
                select pg.Player;

            Player player = players.First();

            return String.Format("Имя = {0}\nСерия = {1}",player.Name,player.BestSeries);
        }

        public static int SumVictory(Game game)
        {
            IEnumerable<Player> players = from pg in game.PlayerGames
                                          select pg.Player;


            //TODO
            int? sum = players.Sum(player => player.CountVictory);
            return sum ?? 0;
        }
    }
}
