﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Krest_nol_core
{
    [JsonObject(MemberSerialization.OptIn)]
    [JsonConverter(typeof (Game))]
    public partial class Game : CustomCreationConverter<Game>
    {
        public const char DefaultCellsPole = 'X';

        public const char OldDefaultCellsPole = ' ';
        public override Game Create(Type objectType)
        {
            /*KrestNolDataContext krestNolData = new KrestNolDataContext();
            Type t;
            krestNolData.Types.InsertOnSubmit(t);
            krestNolData.SubmitChanges();
            //krestNolData.Stats.First().Type.Stats*/
            return new Game();
        }

        public Victory Victory;

        [JsonProperty]
        public Player[] CurrentPlayers { get; set; }

        private int _currentPlayer;
        [JsonProperty]
        public int CurrentPlayer
        {
            get
            {
                return _currentPlayer; 
                
            }
            set
            {
                if (value >= PlayerCount)
                    _currentPlayer = 0;
                else
                    if (value < 0)
                        _currentPlayer = PlayerCount - 1;
                    else
                        _currentPlayer = value;
            }
        }

        /*public Game()
        {
            Victory = new Victory(this);
        }*/

        //[JsonProperty]
        //public int PlayerCount { get; set; }

        //private int _currentPlayer;

        /*[JsonProperty]
        public int CurrentPlayer
        {
            get { return _currentPlayer; }
            set
            {
                int bufValue = value;
                if (bufValue >= PlayerCount)
                    bufValue = 0;
                if (bufValue < 0)
                    bufValue = PlayerCount - 1;
                _currentPlayer = bufValue;
            }
        }*/

        //[JsonProperty]
        //public int SizePole { get; set; }

        //[JsonProperty]
        //public char[][] Pole { get; set; }

        //[JsonProperty]
        //public int WinSequenceLength { get; set; }

        public void LoadGame()
        {
            int zapolPole = 0;
            for (int i = 0; i < SizePole; i++)
                for (int j = 0; j < SizePole; j++)
                {
                    if (Pole[i * SizePole +j] != DefaultCellsPole)
                        zapolPole++;
                }
            if (Victory == null)
                Victory = new Victory(this);
            Victory.ZapolnenostyPole = zapolPole;
            //StartGame();
        }
    }
}
