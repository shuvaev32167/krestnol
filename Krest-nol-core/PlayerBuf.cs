﻿using System;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Krest_nol_core
{
    [JsonObject(MemberSerialization.OptIn)]
    [JsonConverter(typeof(PlayerBuf))]
    public class PlayerBuf : CustomCreationConverter<PlayerBuf>
    {
        public override PlayerBuf Create(Type objectType)
        {
            return new PlayerBuf();
        }

        public PlayerBuf() { }

        public PlayerBuf(Player player)
        {
            BestSeries = player.BestSeries;
            CountGames = player.CountGames;
            CountVictory = player.CountVictory;
            CurrentSeries = player.CurrentSeries;
            ID = player.ID;
            Name = player.Name;
            Symbol = player.Symbol;
        }

        public static implicit operator PlayerBuf(Player player)
        {
            return new PlayerBuf(player);
        }

        public static implicit operator Player(PlayerBuf playerBuf)
        {
            return ToPlayer(playerBuf);
        }

        public static Player ToPlayer(PlayerBuf playerBuf)
        {
            Player player = new Player
            {
                BestSeries = playerBuf.BestSeries,
                CountGames = playerBuf.CountGames,
                CountVictory = playerBuf.CountVictory,
                CurrentSeries = playerBuf.CurrentSeries,
                ID = playerBuf.ID,
                Name = playerBuf.Name,
                Symbol = playerBuf.Symbol
            };
            return player;
        }

        public static PlayerBuf[] FromPlayerArray(Player[] players)
        {
            PlayerBuf[] playerBufs = new PlayerBuf[players.Count()];
            for (int i = 0, size = players.Count(); i < size; i++)
            {
                playerBufs[i] = new PlayerBuf(players[i]);
            }
            return playerBufs;
        }

        public static Player[] ToPlayerArray(PlayerBuf[] playerBufs)
        {
            Player[] players = new Player[playerBufs.Count()];
            for (int i = 0, size = playerBufs.Count(); i < size; i++)
            {
                players[i] = playerBufs[i];
            }
            return players;
        }

        [JsonProperty]
        public int ID { get; set; }
        
        [JsonProperty] 
        public string Name { get; set; }

        [JsonProperty] 
        public int? CountVictory { get; set; }

        [JsonProperty] 
        public int? CurrentSeries { get; set; }

        [JsonProperty] 
        public int? BestSeries { get; set; }

        [JsonProperty] 
        public char Symbol { get; set; }

        [JsonProperty] 
        public int? CountGames { get; set; }
    }
}
