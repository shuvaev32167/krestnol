﻿using System.Linq;

namespace Krest_nol_core
{
    public class DataBase
    {
        public KrestNolDataContext KrestNol { get; private set; }

        public void RefreshWithSubmit()
        {
            if (KrestNol != null) KrestNol.SubmitChanges();
            KrestNol = new KrestNolDataContext();
        }

        public void RefreshWithRollback()
        {
            KrestNol = new KrestNolDataContext();
        }
        
        private DataBase()
        {
            KrestNol = new KrestNolDataContext();
        }

        public static DataBase DB { get; private set; }

        static DataBase()
        {
            DB = new DataBase();
        }

        public void RefreshDataContext()
        {
            KrestNol = new KrestNolDataContext();
        }

        public Game[] GetFullGames()
        {
            return KrestNol.Games.ToArray();
        }
    }
}
