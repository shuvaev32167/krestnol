﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Krest_nol_core
{
    [JsonObject(MemberSerialization.OptIn)]
    [JsonConverter(typeof(SaveBuf))]
    public class SaveBuf : CustomCreationConverter<SaveBuf>
    {
        public override SaveBuf Create(Type objectType)
        {
            return new SaveBuf();
        }

        public SaveBuf() { }

        public SaveBuf(Game game)
        {
            CurrentPlayer = game.CurrentPlayer;
            CurrentPlayerID = game.CurrentPlayerID;
            CurrentPlayers = PlayerBuf.FromPlayerArray(game.CurrentPlayers);
            ID = game.ID;
            PlayerCount = game.PlayerCount;
            Pole = game.Pole;
            SizePole = game.SizePole;
            WinSequenceLength = game.WinSequenceLength;
        }

        public static implicit operator SaveBuf(Game game)
        {
            return new SaveBuf(game);
        }

        public static implicit operator Game(SaveBuf saveBuf)
        {
            return ToGame(saveBuf);
        }

        public static Game ToGame(SaveBuf saveBuf)
        {
            Game game = new Game
            {
                CurrentPlayer = saveBuf.CurrentPlayer,
                CurrentPlayerID = saveBuf.CurrentPlayerID,
                CurrentPlayers = PlayerBuf.ToPlayerArray(saveBuf.CurrentPlayers),
                ID = saveBuf.ID,
                PlayerCount = saveBuf.PlayerCount,
                Pole = saveBuf.Pole,
                SizePole = saveBuf.SizePole,
                WinSequenceLength = saveBuf.WinSequenceLength
            };
            return game;
        }

        [JsonProperty]
        public PlayerBuf[] CurrentPlayers { get; set; }

        [JsonProperty]
        public int CurrentPlayer { get; set; }

        [JsonProperty]
        public int SizePole { get; set; }

        [JsonProperty]
        public string Pole { get; set; }

        [JsonProperty]
        public int WinSequenceLength { get; set; }

        [JsonProperty] 
        public int ID { get; set; }

        [JsonProperty] 
        public int PlayerCount { get; set; }

        [JsonProperty] 
        public int CurrentPlayerID { get; set; }


    }
}
