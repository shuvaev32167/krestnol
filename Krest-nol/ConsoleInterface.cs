﻿using System;
using System.Linq;
using Krest_nol_core;

namespace KrestNol
{
    public class ConsoleInterface
    {
        private const string HorizontalBorder = " -";
        private const string VerticalBorder = "|";
        private const int MaxPole = 10, MinPole = 2, MinPlayer = 1;
        private const char DefaultCellsPole = ' ';

        private Game _game;

        public ConsoleInterface()
        {
            _game = new Game();
        }

        public void NewGame()
        {
            Console.Write("Введите размер поля: ");
            int sizePole;
            Input.Answer parceAnsverResult = Input.ParseInput(Console.ReadLine(), out sizePole, ref _game);
            while (sizePole < MinPole || sizePole > MaxPole)
            {
                if (parceAnsverResult == Input.Answer.Load)
                {
                    StartGame();
                    return;
                }
                if (parceAnsverResult == Input.Answer.Ok || parceAnsverResult == Input.Answer.Error)
                    Console.WriteLine("Не верные размеры поля");
                else
                {
                    Console.Clear();
                    Console.Write("Введите размер поля: ");
                }
                parceAnsverResult = Input.ParseInput(Console.ReadLine(), out sizePole, ref _game);
            }
            _game.SizePole = sizePole;
            Console.Clear();
            Console.Write("Введите число повторений в ряду: ");
            int winSequenceLength;
            parceAnsverResult = Input.ParseInput(Console.ReadLine(), out winSequenceLength, ref _game);
            while (winSequenceLength < MinPole || winSequenceLength > _game.SizePole)
            {
                if (parceAnsverResult == Input.Answer.Load)
                {
                    StartGame();
                    return;
                }
                if (parceAnsverResult == Input.Answer.Ok || parceAnsverResult == Input.Answer.Error)
                    Console.WriteLine("Не верное число повторений");
                else
                {
                    Console.Clear();
                    Console.Write("Введите число повторений в ряду: ");
                }
                parceAnsverResult = Input.ParseInput(Console.ReadLine(), out winSequenceLength, ref _game);
            }
            Console.Clear();
            _game.WinSequenceLength = winSequenceLength;
            Console.Write("Введите число игроков: ");
            int playerCount;
            parceAnsverResult = Input.ParseInput(Console.ReadLine(), out playerCount, ref _game);
            while (playerCount < MinPlayer || playerCount > (_game.SizePole * _game.SizePole) / _game.WinSequenceLength)
            {
                if (parceAnsverResult == Input.Answer.Load)
                {
                    StartGame();
                    return;
                }
                if (parceAnsverResult == Input.Answer.Ok || parceAnsverResult == Input.Answer.Error)
                    Console.WriteLine("Не верное число игроков");
                else
                {
                    Console.Clear();
                    Console.Write("Введите число игроков: ");
                }
                parceAnsverResult = Input.ParseInput(Console.ReadLine(), out playerCount, ref _game);
            }
            Console.Clear();
            DataBase dataBase = DataBase.DB;
            KrestNolDataContext krestNolData = dataBase.KrestNol;
            _game.PlayerCount = playerCount;
            _game.CurrentPlayers = new Player[_game.PlayerCount];
            for (int i = 0; i < _game.PlayerCount; i++)
            {
                Console.WriteLine("Введите имя {0}-го игрока", i);
                string buf;
                Input.ParseInput(Console.ReadLine(), out buf, ref _game);
                if (parceAnsverResult == Input.Answer.Load)
                {
                    StartGame();
                    return;
                }
                Player player = krestNolData.Players.FirstOrDefault(x => x.Name == buf);
                if (player == null)
                {
                    krestNolData.Players.InsertOnSubmit(_game.CurrentPlayers[i]);
                    krestNolData.SubmitChanges();
                }
                _game.CurrentPlayers[i] = player;
                Console.Clear();
            }
            _game.CurrentPlayer = 0;
            _game.Pole = new string(DefaultCellsPole, sizePole * sizePole);
            StartGame();
        }

        public void StartGame()
        {
            DataBase dataBase = DataBase.DB;
            KrestNolDataContext krestNolData = dataBase.KrestNol;
            //TODO
            bool gameNotExists = !krestNolData.Games.Any(g => g.ID == _game.ID);
            if (gameNotExists)
            {
                krestNolData.Games.InsertOnSubmit(_game);
            }
            if (_game.Victory == null)
                _game.Victory = new Victory(_game);
            do
            {
                DisplayPole();
                Console.WriteLine("Ходит {0}", _game.CurrentPlayers[_game.CurrentPlayer].Name);
                Point pos = null;
                Input.Answer parceAnswerResult = Input.ParseInput(Console.ReadLine(), ref pos, ref _game);
                bool isCorrectCoordinatsY = pos != null && (pos.Y >= 0 && pos.Y < _game.SizePole);
                bool isCorrectCoordinatsX = pos != null && (pos.X >= 0 && pos.X < _game.SizePole);
                bool isCorrectCoordinats = isCorrectCoordinatsX && isCorrectCoordinatsY;
                bool isCorrectPos = true;
                if (isCorrectCoordinats)
                    isCorrectPos = (_game.Pole[pos.Y * _game.SizePole + pos.X] == DefaultCellsPole);
                while (!isCorrectCoordinats || !isCorrectPos)
                {
                    bool isAnswerGood = (parceAnswerResult == Input.Answer.Ok ||
                                         parceAnswerResult == Input.Answer.Error);
                    if (isAnswerGood && !isCorrectCoordinats) Console.WriteLine("Не верные координаты");
                    else if (parceAnswerResult != Input.Answer.Stat)
                    {
                        if (!isCorrectPos)
                        {
                            Console.WriteLine("Ячейка занята");
                        }
                        else
                        {
                            Console.Clear();
                            DisplayPole();
                            Console.Write("Ходит {0}\n", _game.CurrentPlayers[_game.CurrentPlayer].Name);
                        }
                    }
                    parceAnswerResult = Input.ParseInput(Console.ReadLine(), ref pos, ref _game);
                    isCorrectCoordinatsY = pos != null && (pos.Y >= 0 && pos.Y < _game.SizePole);
                    isCorrectCoordinatsX = pos != null && (pos.X >= 0 && pos.X < _game.SizePole);
                    isCorrectCoordinats = isCorrectCoordinatsX && isCorrectCoordinatsY;
                    if (pos != null)
                    {
                        int inputPos = pos.Y*_game.SizePole + pos.X;
                        isCorrectPos = !isCorrectCoordinats || (_game.Pole[inputPos] == DefaultCellsPole);
                    }
                    else isCorrectPos = true;
                }

                string pole = _game.Pole;
                _game.CurrentPlayers[_game.CurrentPlayer].MakeAMove(pos, ref pole, _game.SizePole);
                _game.Pole = pole;
                _game.Victory.CalculateVictory(pos);
                _game.CurrentPlayer++;
            } while (!_game.Victory.IsEndOfGame);
            DisplayPole();
            foreach (Player player in _game.CurrentPlayers)
            {
                Player pplayer = krestNolData.Players.SingleOrDefault(x => x.Name == player.Name);
                if (pplayer == null)
                {
                    krestNolData.Players.InsertOnSubmit(player);
                }
                if (player.CountGames == null)
                    player.CountGames = 1;
                else
                    player.CountGames++;
            }
            krestNolData.SubmitChanges();
            if (_game.Victory.IsVictoryPlayer)
            {
                _game.CurrentPlayer--;
                Console.WriteLine("Победил игрок № {0}\nС именем {1}",
                    _game.CurrentPlayer,
                    _game.CurrentPlayers[_game.CurrentPlayer].Name);
                Player player = _game.CurrentPlayers[_game.CurrentPlayer];
                if (player.CountVictory == null)
                    player.CountVictory = 1;
                else
                    player.CountVictory++;
                if (player.CurrentSeries == null)
                    player.CurrentSeries = 1;
                else
                    player.CurrentSeries++;
                if (player.BestSeries < player.CurrentSeries)
                    player.BestSeries = player.CurrentSeries;
                for (int i = 0, size = _game.CurrentPlayers.Count(); i < size; i++)
                {
                    if (i != _game.CurrentPlayer)
                    {
                        if (_game.CurrentPlayers[i].BestSeries < _game.CurrentPlayers[i].CurrentSeries)
                            _game.CurrentPlayers[i].BestSeries = _game.CurrentPlayers[i].CurrentSeries;
                        _game.CurrentPlayers[i].CurrentSeries = 0;
                    }
                }
                _game.VictoryPlayerID = _game.CurrentPlayers[_game.CurrentPlayer].ID;
            }
            else
            {
                Console.WriteLine("Ничья");
                for (int i = 0, size = _game.CurrentPlayers.Count(); i < size; i++)
                {
                    if (_game.CurrentPlayers[i].BestSeries < _game.CurrentPlayers[i].CurrentSeries)
                        _game.CurrentPlayers[i].BestSeries = _game.CurrentPlayers[i].CurrentSeries;
                    _game.CurrentPlayers[i].CurrentSeries = 0;
                }
            }
            Console.ReadLine();
            WorkingBd.Save(_game);
            Environment.Exit(0);
        }

        private void DisplayPole()
        {
            Console.Clear();
            for (int j = 0; j < _game.SizePole; j++)
                Console.Write(HorizontalBorder);
            Console.WriteLine();
            for (int i = 0; i < _game.SizePole; i++)
            {
                Console.Write(VerticalBorder);
                for (int j = 0; j < _game.SizePole; j++)
                    Console.Write(_game.Pole[i * _game.SizePole + j] + VerticalBorder);
                Console.WriteLine();
                for (int j = 0; j < _game.SizePole; j++)
                    Console.Write(HorizontalBorder);
                Console.WriteLine();
            }
        }
    }
}