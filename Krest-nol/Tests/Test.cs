﻿using System.Globalization;
using Krest_nol_core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KrestNol.Tests
{
    [TestClass]
    public class Test
    {
        [TestMethod]
        public void TestParseInputInt()
        {
            Game game = new Game();
            int i;

            const int checkInt = 123;

            Input.Answer answer = Input.ParseInput(checkInt.ToString(CultureInfo.InvariantCulture), out i, ref game);

            const Input.Answer expectedAnswer = Input.Answer.Ok;

            Assert.AreEqual(expectedAnswer, answer,
                "Не удачное преобразование. Ожидалось {0}, фактически {1}", expectedAnswer, answer);

            Assert.AreEqual(checkInt, i,
               "Не удачное преобразование. Ожидалось {0}, фактически {1}", checkInt, i);
        }

        [TestMethod]
        public void TestParseInputStr()
        {
            Game game = new Game();
            string i;

            const string checkStr = "123";

            Input.Answer answer = Input.ParseInput(checkStr, out i, ref game);

            const Input.Answer expectedAnswer = Input.Answer.Ok;

            Assert.AreEqual(expectedAnswer, answer,
                "Не удачное преобразование. Ожидалось {0}, фактически {1}", expectedAnswer, answer);

            Assert.AreEqual(checkStr, i,
               "Не удачное преобразование. Ожидалось {0}, фактически {1}", checkStr, i);
        }

        [TestMethod]
        public void TestParseInputPoint()
        {
            Game game = new Game();
            Cell i = null;

            Cell checkPoint = new Cell(0, 1);

            Input.Answer answer = Input.ParseInput(checkPoint.ToString(), ref i, ref game);

            const Input.Answer expectedAnswer = Input.Answer.Ok;

            Assert.AreEqual(expectedAnswer, answer,
                "Не удачное преобразование. Ожидалось {0}, фактически {1}", expectedAnswer, answer);

            Assert.AreEqual(checkPoint.ToString(), i.ToString(),
               "Не удачное преобразование. Ожидалось {0}, фактически {1}", checkPoint.ToString(), i.ToString());
        }

        [TestMethod]
        public void TestCalculateVictory()
        {
            Game game = new Game();
            Cell i = null;

            Input.ParseInput("loadbd 1", ref i, ref game);

            Victory victory = new Victory(game);

            victory.CalculateVictory(new Cell(1,1));

            bool flag = victory.IsEndOfGame;

            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void TestPlayetMakeAMove()
        {
            Game game = new Game();
            Cell i = null;

            Input.ParseInput("loadbd 1", ref i, ref game);

            string pole = game.Pole;
            game.CurrentPlayers[1].MakeAMove(new Cell(2,2), ref pole, game.SizePole );
        }

        [TestMethod]
        public void TestSaveLoadJosn()
        {
            Game game = new Game();
            ExternalFile.Load("123", ref game);
            ExternalFile.Save("1234", game);
        }

    }
}
