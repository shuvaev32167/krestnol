﻿using System.IO;
using Krest_nol_core;

namespace Addon
{
    public class Addon: PublicLibrary.IStat
    {
        public bool StatSave(Game game, string path)
        {
            var file = new StreamWriter(path);
            file.WriteLine("Число игроков = {0}\nЛучший игрок:\n{1}\nСумма побед = {2}", Stat.CountPlayer(game)
                        , Stat.BestSeries(game), Stat.SumVictory(game));
            file.Close();
            return true;
        }
    }
}
